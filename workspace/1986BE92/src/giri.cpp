#include "giri.hpp"
#include "task.hpp"
#include "message_queue.hpp"
#include "MDR32F9Qx_port.h"
#include "MDR32F9Qx_rst_clk.h"
#include "MDR32F9Qx_uart.h"
#include <string>

#define PIN0 0
#define PIN1 1
#define PIN2 2
#define PIN3 3
#define PIN4 4
#define PIN5 5
#define PIN6 6
#define PIN7 7
#define PIN8 8
#define PIN9 9
#define PIN10 10
#define PIN11 11
#define PIN12 12
#define PIN13 13
#define PIN14 14
#define PIN15 15

#define PIN_0 (1<<0)
#define PIN_1 (1<<1)
#define PIN_2 (1<<2)
#define PIN_3 (1<<3)
#define PIN_4 (1<<4)
#define PIN_5 (1<<5)
#define PIN_6 (1<<6)
#define PIN_7 (1<<7)
#define PIN_8 (1<<8)
#define PIN_9 (1<<9)
#define PIN_10 (1<<10)
#define PIN_11 (1<<11)
#define PIN_12 (1<<12)
#define PIN_13 (1<<13)
#define PIN_14 (1<<14)
#define PIN_15 (1<<15)

#define PCLK_BIT(BASE) ((uint32_t)(1 << ((((uint32_t)BASE) >> 15) & 0x1F)))
MessageQueue<int> message(100);

class Uart: public Task
{
public:
	Uart(): Task("Uart"), result(0), state(false)
	{
		MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PCLK_EN_UART2; 
		MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PCLK_EN_PORTF;
		MDR_RST_CLK->UART_CLOCK |= RST_CLK_UART_CLOCK_UART2_CLK_EN;
		UART_BRGInit(MDR_UART2, UART_HCLKdiv1);

		MDR_PORTF->RXTX &= ~(PIN_1);
		MDR_PORTF->OE |= (PIN_1);
		MDR_PORTF->FUNC |= (3 << (2 * PIN1));
		MDR_PORTF->ANALOG |= (PIN_1);
		MDR_PORTF->PULL &= ~(1 << (16 + PIN1) | PIN_1);
		MDR_PORTF->PD &= ~(1 << (16 + PIN1) | PIN_1);
		MDR_PORTF->PWR |= (3 << (2 * PIN1));
		MDR_PORTF->GFEN &= ~(PIN_1);

		MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PCLK_EN_PORTB;
		MDR_PORTB->RXTX &= ~(PIN_9);
		MDR_PORTB->OE |= (PIN_9);
		MDR_PORTB->FUNC &= ~(3 << (2 * PIN9));
		MDR_PORTB->ANALOG |= (PIN_9);
		MDR_PORTB->PULL &= ~(1 << (16 + PIN9));
		MDR_PORTB->PD &= ~(1 << (16 + PIN9) | PIN_9);
		MDR_PORTB->PWR |= (3 << (2 * PIN9));
		MDR_PORTB->GFEN &= ~(PIN_9);

		UART_InitTypeDef uart_ini;
		uart_ini.UART_BaudRate = 115200;
		uart_ini.UART_FIFOMode = UART_FIFO_OFF;
		uart_ini.UART_HardwareFlowControl = UART_HardwareFlowControl_TXE | UART_HardwareFlowControl_RXE;
		uart_ini.UART_Parity = UART_Parity_No;
		uart_ini.UART_StopBits = UART_StopBits1;
		uart_ini.UART_WordLength = UART_WordLength8b;
		UART_Init(MDR_UART2, &uart_ini);		
		UART_Cmd(MDR_UART2, ENABLE);
		ID = "8888888888888888";
		
		const char* mode1 = "at+mode=1\r\n\0";
		const char* mode2 = "at+rf_config=867700000,10,0,1,8,14\r\n\0";

		while(*mode1)
		{
			while(UART_GetFlagStatus (MDR_UART2, UART_FLAG_BUSY)){}
			UART_SendData(MDR_UART2, *(mode1++));
		}

		Delay(2000);

		while(*mode2)
		{
			while(UART_GetFlagStatus (MDR_UART2, UART_FLAG_BUSY)){}
			UART_SendData(MDR_UART2, *(mode2++));
		}
	}

private:
	uint32_t result;
	std::string ID;
	bool state;
	virtual void Execute()
	{
		int command = 0;
		const char* mode0 = "at+txc=1,25,"; //ID8888888888888888COM1P1000END
		const char* mode1 = "1"; //1P
		const char* mode2 = "2";
		const char* mode3 = "3";
		const char* mode4 = "4";
		const char* m = nullptr;
		std::string str = "";
		while(1)
		{
			str = ID;
			message.Pop(command);
			
			if (command == 1)
			{
				result %= 10000;
				result += 1;
				str += mode1;
			}
			else if (command == 2)
			{
				if (result)
					result -= 1;
				str += mode2;
			}
			else if (command == 3)
			{
				state = !state;
				str += mode3;
			} 
			if (command == 4)
			{			
				str += mode4;
			}
			if ((result / 1000) > 0)
				str += "";
			else if ((result / 100) > 0)
				str += "0";
			else if ((result / 10) > 0)
				str += "00";
			else 
				str += "000";

			str += std::to_string(result);

			if (command == 4)
			{
				result = 0;
				MDR_PORTB->RXTX ^= PIN_9;
				Delay(100);
				MDR_PORTB->RXTX ^= PIN_9;
			}

			str = string_to_hex(str);

			str = mode0 + str + "\r\n";
			m = str.c_str();
			while(*m)
			{					
				while(UART_GetFlagStatus (MDR_UART2, UART_FLAG_BUSY)){}
				UART_SendData(MDR_UART2, *(m++));
			}
			Delay(300);
		}
	}

	std::string string_to_hex(const std::string& input)
	{
	    static const char* const lut = "0123456789ABCDEF";
	    size_t len = input.length();

	    std::string output;
	    output.reserve(2 * len);
	    for (size_t i = 0; i < len; ++i)
	    {
	        const unsigned char c = input[i];
	        output.push_back(lut[c >> 4]);
	        output.push_back(lut[c & 15]);
	    }
	    return output;
	}
};

class ButtonProcessing: public Task
{
public:
	ButtonProcessing(MDR_PORT_TypeDef* port, uint32_t pin, int command): Task("ButtonProcessing"), btn_port(port), btn_pin(pin), btn_command(command)
	{
		/*---------------------Инициализация кнопки---------------------*/
		MDR_RST_CLK->PER_CLOCK |= PCLK_BIT(port); // Тактирование порта

		port->RXTX &= ~(1 << pin);// & 0xFFFF; // Данные для выдачи на выводы порта и для чтения - 0
		port->OE &= ~(1 << pin);// & 0xFFFF; // Направление передачи данных на выводах порта - вход
		port->FUNC &= ~(3 << (2 * pin));// & 0xFFFFFFFF; // Режим работы вывода порта - порт
		port->ANALOG |= (1 << pin);// | 0x0000; // Режим работы контроллера - цифровой
		port->PULL |= (1 << (16 + pin));// | 0x00000000; // Разрешение подтяжки вверх
		port->PULL &= ~(1 << pin);// & 0xFFFFFFFF; // Запрет подтяжки вниз
		port->PD &= ~(1 << (16 + pin));// & 0xFFFFFFFF; // Режим работы входа - триггер Шмитта включен
		port->PD &= ~(1 << pin);// & 0xFFFFFFFF; // Режим работы выхода - управляемый драйвер
		port->PWR |= (3 << (2 * pin));// & 0x00000000; // Режим работы вывода порта -  быстрый фронт 
		port->GFEN &= ~(1 << pin);// & 0xFFFF; // Режим работы входного фильтра - фильтр выключен

		/*---------------------Инициализация кнопки---------------------*/


		/*---------------------Инициализация диода---------------------*/
		MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PCLK_EN_PORTB;
		MDR_PORTB->RXTX &= ~(PIN_8);
		MDR_PORTB->OE |= (PIN_8);
		MDR_PORTB->FUNC &= ~(3 << (2 * PIN8));
		MDR_PORTB->ANALOG |= (PIN_8);
		MDR_PORTB->PULL &= ~(1 << (16 + PIN8));
		MDR_PORTB->PD &= ~(1 << (16 + PIN8) | PIN_8);
		MDR_PORTB->PWR |= (3 << (2 * PIN8));
		MDR_PORTB->GFEN &= ~(PIN_8);
		/*---------------------Инициализация диода---------------------*/
	}

private:
	MDR_PORT_TypeDef* btn_port;
	uint32_t btn_pin;
	uint32_t btn_flag;
	int btn_command;

	virtual void Execute()
	{
		btn_flag = 0;
		while(1)
		{
			if ((btn_port->RXTX & (1 << btn_pin)) != 0)
			{
				btn_flag |= (1 << 31);
			}
			else
			{
				if ((btn_flag & 0xFF) >= 12)
				{
					MDR_PORTB->RXTX ^= PIN_8;
					Delay(10);
					MDR_PORTB->RXTX ^= PIN_8;
					message.Push(btn_command, INFINITE_TIMEOUT);
				}
				btn_flag = 0;
			}

			if ((btn_flag & (1 << 31)))
				btn_flag++;

			Delay(10);
		}
	}
};

void giri::Initialize()
{
	Task::Add(new Uart(), Task::PriorityNormal, 0x100);
	Task::Add(new ButtonProcessing(MDR_PORTB, 10, 1), Task::PriorityNormal, 0x100);//+
	Task::Add(new ButtonProcessing(MDR_PORTA, 1, 1), Task::PriorityNormal, 0x100);//+
	Task::Add(new ButtonProcessing(MDR_PORTA, 2, 2), Task::PriorityNormal, 0x100);//-
	Task::Add(new ButtonProcessing(MDR_PORTA, 3, 3), Task::PriorityNormal, 0x100);//start
	Task::Add(new ButtonProcessing(MDR_PORTA, 4, 4), Task::PriorityNormal, 0x100);//new
}










//UART(serial2): PF1 - TX, PF0 - RX
/*
Тактирование порта Ф
Тактирование юарта
-------ТХ----------------
RXTX = 0 //Изначальные данные = 0
OE = 1 //Порт работает на выход
FUNC = 11 //Режим работы - переопределенный
ANALOG = 1 //Режим работы - цифровой
PULL = (31-16)0, (15-0)0 //Подтяжка вверх выключена, вниз - выключена
PD = (31-16)0, (15-0)0 //Триггер Шмитта выключен, режим работы выхода - управляемый драйвер
PWR = 11 //Максимально быстрый фронт
GFEN = 0 //Фильтр импульсов выключен
-------RХ----------------
RXTX = 0 //Изначальные данные = 0
OE = 0 //Порт работает на вход
FUNC = 11 //Режим работы - порт
ANALOG = 1 //Режим работы - переопределенный
PULL = (31-16)0, (15-0)0 //Подтяжка вверх выключена, вниз - выключена
PD = (31-16)0, (15-0)0 //Триггер Шмитта выключен, режим работы выхода - управляемый драйвер
PWR = 11 //Максимально быстрый фронт
GFEN = 0 //Фильтр импульсов выключен
*/

//Button: PA1, PA2, PA3, PA4 (PB10 - test)
/*
Тактирование порта А, тактирование порта Б
RXTX = 0 //Изначальные данные = 0
OE = 0 //Порт работает на вход
FUNC = 00 //Режим работы - порт
ANALOG = 1 //Режим работы - цифровой
PULL = (31-16)1, (15-0)0 //Подтяжка вверх включена, вниз - выключена
PD = (31-16)1, (15-0)0 //Триггер Шмитта включен, режим работы выхода - управляемый драйвер
PWR = 11 //Максимально быстрый фронт
GFEN = 1 //Фильтр импульсов включен
*/

//Led: PB8 - green, PB9 - red
/*
Тактирование порта Б
RXTX = 0 //Изначальные данные = 0
OE = 1 //Порт работает на выход
FUNC = 00 //Режим работы - порт
ANALOG = 1 //Режим работы - цифровой
PULL = (31-16)0, (15-0)0 //Подтяжка вверх выключена, вниз - выключена
PD = (31-16)0, (15-0)0 //Триггер Шмитта выключен, режим работы выхода - управляемый драйвер
PWR = 11 //Максимально быстрый фронт
GFEN = 0 //Фильтр импульсов выключен
*/

/*

at+mode=1\r\n\0
OK

at+get_config=dev_eui
OK3037343644357402

at+rf_config=867700000,10,0,1,8,14\r\n\0
OK

at+txc=4,25,aaaa\r\n\0
at+recv=9,0,0

at+rxc=1
OK
at+recv=0,2,10,30313233343536373839
*/
//at+recv=0,2,,  ID8888888888888888COM01R1000END

/* Using commands
at+mode
at+rf_config
at+txc
at+rxc
at+tx_stop
at+rx_stop
at+get_config=dev_eui
*/

		/*
		MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PCLK_EN_UART2; 
		MDR_RST_CLK->UART_CLOCK |= RST_CLK_UART_CLOCK_UART2_CLK_EN;
		MDR_RST_CLK->UART_CLOCK = tmpreg;
		UARTx->IBRD = integerdivider;
		UARTx->FBRD = fractionaldivider;
		UARTx->LCR_H = tmpreg;
		UARTx->CR = tmpreg;
		UARTx->CR |= CR_EN_Set;
		UARTx->DR = (Data & (uint16_t)0x0FF);
		UARTx->FR & UART_FLAG

		typedef struct
		{
		  __IO uint32_t DR;
		  __IO uint32_t RSR_ECR;
		       uint32_t RESERVED0[4];
		  __IO uint32_t FR;
		       uint32_t RESERVED1;
		  __IO uint32_t ILPR;
		  __IO uint32_t IBRD;
		  __IO uint32_t FBRD;
		  __IO uint32_t LCR_H;
		  __IO uint32_t CR;
		  __IO uint32_t IFLS;
		  __IO uint32_t IMSC;
		  __IO uint32_t RIS;
		  __IO uint32_t MIS;
		  __IO uint32_t ICR;
		  __IO uint32_t DMACR;
		}MDR_UART_TypeDef;*/