/** @copyright AstroSoft Ltd */
#pragma once

#define MACS_USE_MPU             1
#define MACS_MPU_PROTECT_NULL    1
#define MACS_MPU_PROTECT_STACK   1
