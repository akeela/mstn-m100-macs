def tobin(arg):
	binary = str(bin(int(arg, 16)))
	binary = binary[2:]

	if len(binary) % 4 != 0:
		for i in range (0, 4 - len(binary) % 4):
			binary = "0" + binary

	result = arg + " = "
	for i in range(0, len(binary)):
		result += str(binary[i])
		if ((i+1) % 4 == 0):
			result += " "
	print(result)

def tohex(arg):
	result = str(hex(int(arg, 2)))
	print(result)

while (True):
	c = input()
	if c[0] == "h":
		c = "0x" + c[1:]
		tobin(c)
	else:
		c = "0b" + c
		tohex(c)
